export BASH_SILENCE_DEPRECATION_WARNING=1

if [ -r ~/.bashrc ]; then
	source ~/.bashrc
fi

if [ -f "/opt/homebrew/bin/brew" ]; then
	eval "$(/opt/homebrew/bin/brew shellenv)"
fi

if [ ! -z "${HOMEBREW_PREFIX}" ]; then
	export PATH="${HOMEBREW_PREFIX}/opt/make/libexec/gnubin:$PATH"
fi

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/Users/swvasquez/miniconda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/Users/swvasquez/miniconda/etc/profile.d/conda.sh" ]; then
        . "/Users/swvasquez/miniconda/etc/profile.d/conda.sh"
    else
        export PATH="/Users/swvasquez/miniconda/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

# >>> pyenv >>>
eval "$(direnv hook bash)"
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
# <<< pyenv <<<

