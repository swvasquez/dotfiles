#!/bin/bash -

bash-installer () {
    local OPTIND
    while getopts "u:d:o:" opt; do
        case "${opt}" in
            u)
                url=${OPTARG}
                ;;
            d)
                dest=${OPTARG}
                ;;
            o)
                options=${OPTARG}
                ;;
        esac
    done

	install_cmd="bash ${dest}/${url##*/}"
	[ -n "${options}" ] && install_cmd="${install_cmd} ${options}"

    [ -d "${dest}" ] || mkdir -p ${dest}
    
	wget ${url} -O ${dest}/${url##*/}
    
	echo "Running: ${install_cmd}"
	eval "${install_cmd}"

	rm -f "${dest}/${url##*/}"
}

export -f bash-installer

/bin/bash "${@}"