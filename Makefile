VERBOSE=false
ifneq (${VERBOSE},true)
.SILENT:
endif

.ONESHELL:
MAKEFLAGS += --always-make

SHELL := ./utils.sh

STOW_SRC := ${HOME}/dotfiles
STOW_DEST := ${HOME}
SSH_DIR := ~/.ssh

################################### CORE SETUP ##################################

setup: homebrew stow bash make git 
	printf '\n%s\n\n' "Run source ~/.bash_profile for changes to go into effect."

bash:
	chsh -s /bin/bash
	stow --dir ${STOW_SRC} -t ${STOW_DEST} bash

stow:
	eval "$$(/opt/homebrew/bin/brew shellenv)" && brew install stow

homebrew:
	source=https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh; \
	/bin/bash -c "$$(curl -fsSL $${source})"

make:
	eval "$$(/opt/homebrew/bin/brew shellenv)" && brew install make

git:
	eval "$$(/opt/homebrew/bin/brew shellenv)" && brew install git 
	stow --dir ${STOW_SRC} -t ${STOW_DEST} git

github: github-cli github-cli-ssh

github-cli:
	brew install gh
	brew upgrade gh

github-cli-ssh: GIT_EMAIL := $(shell git config user.email)
github-cli-ssh: SSH_KEYFILE := ${SSH_DIR}/id_github
github-cli-ssh: github-cli
	mkdir -p ${SSH_DIR} $$(dirname ${SSH_KEYFILE})
	ssh-keygen -t ed25519 -C ${GIT_EMAIL} -f ${SSH_KEYFILE}
	cat <<- EOF >> ${SSH_DIR}/config
	Host *.github.com
		AddKeysToAgent yes
		UseKeychain yes
		IdentityFile ${SSH_KEYFILE}
	EOF
	ssh-add --apple-use-keychain ${SSH_KEYFILE}
	gh auth login

gpg:
	eval "$$(/opt/homebrew/bin/brew shellenv)" && brew install gnupg

python: PYTHON_VER := 3.10.4
python:
	brew install pyenv
	pyenv install ${PYTHON_VER}

################################### À LA CARTE ##################################

conda:
	url=https://repo.anaconda.com/miniconda/Miniconda3-py39_4.12.0-MacOSX-arm64.sh
	dest=${HOME}
	options="-u -p ${HOME}/miniconda"
	bash-installer -u $${url} -d $${dest} -o "$${options}"

direnv:
	brew install direnv

fzf:
	brew install fzf

go:
	brew install golang

pass:
	brew install pass

password-store:
	pass init ${GPG_ID}
	pass git init

wget:
	brew install wget
