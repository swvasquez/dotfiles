# Dotfiles
This repository contains user-specific configuration files.

## Setup
To install core tooling, run: `make setup`.

## System
This code is for use on machines running *macOS*. It is currently developed on a machine running:

```
System Version: macOS 12.3.1 (21E258)
Kernel Version: Darwin 21.4.0
```



